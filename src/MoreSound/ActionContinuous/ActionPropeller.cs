﻿using UnityEngine;

namespace MoreSound {
    /// <summary>
    /// ActionContinuous implementation for PropellorController types.
    /// </summary>
    class ActionPropeller : ActionContinuous {

        public static ActionPropeller Add(BlockBehaviour b) {
            return ActionContinuous.Add<ActionPropeller>(b,
                sound: (b.transform.localScale.z > 0.2f) ? Sound.propSound : Sound.jetSound,
                volume: (b.transform.localScale.z > 0.2f) ? 0.3f : 0.07f,
                pitch: 1.5f,
                fadeDelta: 2);
        }

        public static ActionPropeller AddHeli(BlockBehaviour b) {
            return ActionContinuous.Add<ActionPropeller>(b,
                sound: Sound.heliSound,
                volume: 0.4f,
                pitch: 1.5f,
                fadeDelta: 2);
        }

        protected override bool IsPlaying() {
            return block.Rigidbody.angularVelocity.sqrMagnitude > (20f * 20f);
        }

        protected override void UpdateAlways() {
            PitchReal = Mathf.Lerp(PitchReal,
                Mathf.Clamp((block.Rigidbody.angularVelocity.magnitude / (source.clip == (Sound.propSound || Sound.heliSound) ? 30f : 50f)), 0, pitchBase),
                Time.deltaTime);
        }
    }
}
