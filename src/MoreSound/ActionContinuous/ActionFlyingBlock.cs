﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MoreSound {

    /// <summary>
    /// ActionContinuous implementation for FlyingController types.
    /// </summary>
    class ActionFlyingBlock : ActionContinuous {

        public static ActionFlyingBlock Add(BlockBehaviour b) {
            float power = Mathf.Abs((b.BuildingBlock as FlyingController).SpeedSlider.Value);
            if (power > 1E20) {
                return ActionContinuous.Add<ActionFlyingBlock>(b, 
                    sound: Sound.negativeWaterCannonSound, 
                    volume: 0.1f, 
                    pitch: 1f, 
                    fadeDelta: 6f);
            }
            else {
                return ActionContinuous.Add<ActionFlyingBlock>(b, 
                    sound: (power <= 2f) ? Sound.flyingBlockSound : Sound.vacuumSound, 
                    volume: (power <= 2f) ? 0.4f : 0.2f, 
                    pitch: Mathf.Clamp(power, 0f, 1.2f), 
                    fadeDelta: 6);
            }
        }

        protected override bool IsPlaying() {
            return (block as FlyingController).flying;
        }
    }
}
