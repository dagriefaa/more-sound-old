﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MoreSound {
    /// <summary>
    /// ActionContinuous implementation for SliderCompress types.
    /// </summary>
    class ActionPiston : ActionContinuous {

        SliderCompress piston;

        float lastPosToBe;

        float soundTime = 0f;

        public static ActionPiston Add(BlockBehaviour b) {
            return ActionContinuous.Add<ActionPiston>(b,
                sound: Sound.servoSound,
                volume: 0.15f * Mathf.Clamp(b.transform.localScale.magnitude, 0, 1),
                pitch: Mathf.Clamp((b.BuildingBlock as SliderCompress).SpeedSlider.Value, 0.3f, 3f),
                fadeDelta: 8);
        }

        void Start() {
            piston = block as SliderCompress;
        }

        protected override void UpdateAlways() {
            if (soundTime > 0.01f) { 
                soundTime = Mathf.Lerp(soundTime, 0, Time.deltaTime * piston.lerpSpeed * piston.SpeedSlider.Value);
            }
            
            if (!Mathf.Approximately(piston.posToBe, lastPosToBe) && soundTime < 0.01f) {
                soundTime += 1;
            }
            lastPosToBe = piston.posToBe;
        }

        protected override bool IsPlaying() {
            return soundTime > 0.01f;
        }
    }
}
