﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MoreSound {
    /// <summary>
    /// ActionContinuous implementation for CogMotorControllerHinge types.
    /// </summary>
    class ActionMotor : ActionContinuous {

        float movement;

        public static ActionMotor Add(BlockBehaviour b, AudioClip sound, float volume = 0.4f, float pitch = 1f, float fadeDelta = 3f) {
            return ActionContinuous.Add<ActionMotor>(b, sound, volume, pitch, fadeDelta);
        }

        protected override bool IsPlaying() {
            CogMotorControllerHinge self = block as CogMotorControllerHinge;
            movement = Mathf.Abs(transform.InverseTransformDirection(block.Rigidbody.angularVelocity).z);
            return self.Velocity != 0f && (movement > 0.01f || self.speedSlider.Value > 4f);
        }

        protected override void UpdateAlways() {
            PitchReal = Mathf.Lerp(PitchReal,
                Mathf.Clamp((movement / 10.47f), 0, pitchBase), 
                Time.deltaTime);
        }
    }
}
