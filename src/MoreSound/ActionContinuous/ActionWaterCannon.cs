﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MoreSound {

    /// <summary>
    /// ActionContinuous implementation for WaterCannonController types.
    /// </summary>
    class ActionWaterCannon : ActionContinuous {

        enum WaterCannonState {
            Negative,   // negative sound constant
            Normal,     // water sound unheated
            Heated,     // steam sound heated
            Overpowered // steam sound constant
        };
        WaterCannonState cannonState;

        public static ActionWaterCannon Add(BlockBehaviour b) {
            ActionWaterCannon self;

            float power = (b.BuildingBlock as WaterCannonController).StrengthSlider.Value;
            if (power < 0) {
                self = ActionContinuous.Add<ActionWaterCannon>(b,
                    sound: Sound.negativeWaterCannonSound,
                    volume: Mathf.Clamp(-power * 0.005f, 0.05f, 0.2f),
                    fadeDelta: 6);
                self.cannonState = WaterCannonState.Negative;
            }
            else if (power > 1E20) {
                self = ActionContinuous.Add<ActionWaterCannon>(b,
                    sound: Sound.negativeWaterCannonSound,
                    volume: 0.1f,
                    fadeDelta: 6);
                self.cannonState = WaterCannonState.Overpowered;
            }
            else if (power > 50) {
                self = ActionContinuous.Add<ActionWaterCannon>(b,
                    sound: Sound.steamCannonSound,
                    pitch: Mathf.Clamp(0.75f + ((power - 49) * 0.1f), 0.0f, 3f),
                    fadeDelta: 6);
                self.cannonState = WaterCannonState.Overpowered;
            }
            else {
                self = ActionContinuous.Add<ActionWaterCannon>(b,
                    sound: Sound.waterCannonSound,
                    volume: Mathf.Clamp(power / 3f, 0.2f, 0.4f),
                    pitch: Mathf.Clamp(0.75f + (power * 0.25f), 0.0f, 2f),
                    fadeDelta: 6);
                self.cannonState = WaterCannonState.Normal;
            }

            return self;
        }

        protected override bool IsPlaying() {
            return (block as WaterCannonController).isActive;
        }

        protected override void UpdatePlaying() {

            if (cannonState == WaterCannonState.Normal && (block as WaterCannonController).boiling) {
                cannonState = WaterCannonState.Heated;
                source.clip = Sound.steamCannonSound;
                source.Play();
            }
            else if (cannonState == WaterCannonState.Heated && !(block as WaterCannonController).boiling) {
                cannonState = WaterCannonState.Normal;
                source.clip = Sound.waterCannonSound;
                source.Play();
            }
        }
    }
}
