﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MoreSound {
    /// <summary>
    /// ActionContinuous implementation for SteeringWheel types.
    /// </summary>
    class ActionSteering : ActionContinuous {

        float lastAngle = 0f;

        public static ActionSteering Add(BlockBehaviour b) {
            return ActionContinuous.Add<ActionSteering>(b,
                sound: Sound.servoSound, 
                volume: 0.1f * Mathf.Clamp(b.transform.localScale.magnitude, 0, 1),
                pitch: Mathf.Clamp((b.BuildingBlock as SteeringWheel).SpeedSlider.Value, 0.0f, 3f),
                fadeDelta: 8);
        }

        protected override bool IsPlaying() {
            SteeringWheel steeringSelf = block as SteeringWheel;
            bool isPlaying = steeringSelf.AngleToBe != lastAngle && Mathf.Abs(steeringSelf.AngleToBe - lastAngle) > 0.5f;
            lastAngle = steeringSelf.AngleToBe;

            return isPlaying;
        }
    }
}
