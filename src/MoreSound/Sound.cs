﻿using Modding;
using System;
using UnityEngine;

namespace MoreSound {
    /// <summary>
    /// Base class for all sounds.
    /// </summary>
    abstract class Sound : MonoBehaviour {

        // data variables
        protected BlockBehaviour block;
        protected AudioSource source;
        protected float volumeBase;
        protected float pitchBase;

        // state variables
        protected bool soundWasPlaying = false;
        public float DistanceCutoff { get; protected set; }     // Cutoff distance (squared) of sound based on volume.
        public float FadeLevel { get; protected set; }          // Current volume fade fraction.
        public float VolumeReal { get; protected set; }         // Calculated real volume from attenuation, fade, etc.
        public float PitchReal { get; protected set; }          // Unscaled pitch

        // constants
        public static readonly AnimationCurve CONSTANT_CURVE = AnimationCurve.Linear(0, 1, 1, 1);
        public static readonly float REFERENCE_VOLUME = 0.2f;
        public static readonly float REFERENCE_DISTANCE = 500f;

        public static readonly float ACTIVE_VOLUME_MAX = 2f;

        // getters
        static Transform _mainCamera;
        public static Transform MainCamera {
            get {
                if (!_mainCamera) {
                    _mainCamera = Camera.main.transform;
                }
                return _mainCamera;
            }
        }

        // global control variables
        public static float ActiveVolumeTotal = 0f; // total volume of all active audio clips

        // break sounds
        public static AudioClip metalSnapLow;
        public static AudioClip metalSnapMid;
        public static AudioClip metalSnapHigh;
        public static AudioClip metalSnapMixed;
        public static AudioClip woodSnapLow;
        public static AudioClip woodSnapMid;
        public static AudioClip woodSnapHigh;
        public static AudioClip glassSmash;

        // impact sounds
        public static AudioClip woodImpactLight;
        public static AudioClip woodImpactHeavy;
        public static AudioClip metalImpactLight;
        public static AudioClip metalImpactHeavy;

        // action sounds
        public static AudioClip servoSound;
        public static AudioClip motorSound;
        public static AudioClip motorSoundSmall;
        public static AudioClip jetSound;
        public static AudioClip propSound;
        public static AudioClip heliSound;
        public static AudioClip vacuumSound;
        public static AudioClip flamethrowerSoundLow;
        public static AudioClip flamethrowerSoundHigh;
        public static AudioClip waterCannonSound;
        public static AudioClip steamCannonSound;
        public static AudioClip negativeWaterCannonSound;
        public static AudioClip flyingBlockSound;

        public static AudioClip fireSound;
        public static AudioClip rollingSound;

        public static AudioClip laserSound;

        // cannon sounds
        public static AudioClip gunSound;
        public static AudioClip cannonSound;

        /// <summary>
        /// Loads all audio clips for the More Sound mod.
        /// </summary>
        public static void LoadResources() {
            metalSnapHigh = ModResource.GetAudioClip("metal-snap-high");
            metalSnapMid = ModResource.GetAudioClip("metal-snap-mid");
            metalSnapLow = ModResource.GetAudioClip("metal-snap-low");
            metalSnapMixed = ModResource.GetAudioClip("metal-snap-mixed");
            woodSnapHigh = ModResource.GetAudioClip("wood-snap-high");
            woodSnapMid = ModResource.GetAudioClip("wood-snap-mid");
            woodSnapLow = ModResource.GetAudioClip("wood-snap-low");
            glassSmash = ModResource.GetAudioClip("glass-smash");

            woodImpactLight = ModResource.GetAudioClip("wood-impact-light");
            woodImpactHeavy = ModResource.GetAudioClip("wood-impact-heavy");
            metalImpactLight = ModResource.GetAudioClip("metal-impact-light");
            metalImpactHeavy = ModResource.GetAudioClip("metal-impact-heavy");

            servoSound = ModResource.GetAudioClip("servo");
            motorSound = ModResource.GetAudioClip("motor");
            motorSoundSmall = ModResource.GetAudioClip("motor-small");
            vacuumSound = ModResource.GetAudioClip("vacuum");
            jetSound = ModResource.GetAudioClip("jet");
            propSound = ModResource.GetAudioClip("propeller");
            heliSound = ModResource.GetAudioClip("heli");
            flamethrowerSoundLow = ModResource.GetAudioClip("flamethrower-low");
            flamethrowerSoundHigh = ModResource.GetAudioClip("flamethrower-high");
            waterCannonSound = ModResource.GetAudioClip("water");
            steamCannonSound = ModResource.GetAudioClip("steam");
            negativeWaterCannonSound = ModResource.GetAudioClip("magic");
            flyingBlockSound = ModResource.GetAudioClip("flying-block");

            fireSound = ModResource.GetAudioClip("fire");
            rollingSound = ModResource.GetAudioClip("rolling");

            gunSound = ModResource.GetAudioClip("gun");
            cannonSound = ModResource.GetAudioClip("cannon");

            laserSound = ModResource.GetAudioClip("tls");
        }

        /// <summary>
        /// Add a new Action component (of subclass type T) to a block with specified parameters.
        /// </summary>
        /// <typeparam name="T">Subclass to instantiate.</typeparam>
        /// <param name="b">Block behaviour to instantiate on.</param>
        /// <param name="sound">Action audio clip.</param>
        /// <param name="volume">Base volume.</param>
        /// <param name="pitch">Base pitch.</param>
        /// <param name="fadeDelta">Reciprocal of fade in/out time in seconds.</param>
        /// <param name="source">Existing audio source, if required</param>
        /// <param name="loop">Set sound looping</param>
        /// <returns></returns>
        protected static T Add<T>(BlockBehaviour b, AudioClip sound, float volume = 0.4f, float pitch = 1f, AudioSource source = null, bool loop = true) where T : Sound {

            GameObject soundParent = b.transform.FindChild("Sounds")?.gameObject;
            if (!soundParent) {
                soundParent = new GameObject("Sounds");
                soundParent.transform.SetParent(b.transform, false);
            }

            T self = b.gameObject.AddComponent<T>();

            self.block = b;
            self.volumeBase = volume;
            self.PitchReal = self.pitchBase = Mathf.Max(pitch, 0.001f) + UnityEngine.Random.Range(-0.2f, 0.2f);

            self.source = source ?? soundParent.AddComponent<AudioSource>();
            self.source.clip = sound;
            self.source.volume = 0;
            self.source.pitch = self.pitchBase;
            self.source.loop = loop;
            self.source.spatialBlend = 1;
            self.source.rolloffMode = AudioRolloffMode.Custom;
            self.source.SetCustomCurve(AudioSourceCurveType.CustomRolloff, Sound.CONSTANT_CURVE);

            self.DistanceCutoff = volume * Sound.REFERENCE_DISTANCE / Sound.REFERENCE_VOLUME;
            self.DistanceCutoff *= self.DistanceCutoff;

            return self;
        }

        /// <summary>
        /// Checks if sound should be played at all based on distance.
        /// </summary>
        /// <returns>True if sound can really be heard.</returns>
        public bool IsAudible() {
            return (Vector3.SqrMagnitude(transform.position - Sound.MainCamera.position) < DistanceCutoff);
        }

        /// <summary>
        /// Returns the fraction of actual volume required to maintain a constant global maximum.
        /// </summary>
        /// <returns>Volume multiplier between 1 and 0</returns>
        public static float Saturation() {
            if (ActiveVolumeTotal <= ACTIVE_VOLUME_MAX) {
                return 1f;
            }
            return Mathf.Clamp(ACTIVE_VOLUME_MAX / Mathf.Sqrt(ActiveVolumeTotal), 0, 1);
        }

        /// <summary>
        /// Realistic volume rolloff based on distance from camera.
        /// </summary>
        /// <param name="volume">Base volume</param>
        /// <returns>Attenuation multiplier between 1 and 0</returns>
        public float Attenuation(float volume) {
            float x = Vector3.Magnitude(this.transform.position - MainCamera.position) / (volume * (1 / REFERENCE_VOLUME));
            if (x <= 1)
                return 1;
            return Mathf.Clamp((2f / ((float)Math.Log(x))) - 0.3118f, 0, 1);
        }

        private void OnDisable() {
            source.Stop();
            Sound.ActiveVolumeTotal -= VolumeReal;
            VolumeReal = 0f;
            soundWasPlaying = false;
            OnDisableSound();

        }

        protected abstract void Update();

        protected virtual void OnDisableSound() {
        }
    }
}
