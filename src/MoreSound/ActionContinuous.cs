﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MoreSound {

    /// <summary>
    /// Base class for block state sounds which loop (e.g. water cannon).
    /// </summary>
    abstract class ActionContinuous : Sound {

        float fadeDelta;

        protected bool soundStarted = false;

        protected static T Add<T>(BlockBehaviour b, AudioClip sound, float volume = 0.4f, float pitch = 1f, float fadeDelta = 3f) where T : ActionContinuous {
            T res = Sound.Add<T>(b, sound, volume, pitch);
            res.fadeDelta = fadeDelta;
            return res;
        }

        /// <summary>
        /// Returns the action state of the block.
        /// </summary>
        /// <returns>True if block is active.</returns>
        protected abstract bool IsPlaying();

        /// <summary>
        /// Used to always update sound.
        /// Called after IsPlaying.
        /// </summary>
        protected virtual void UpdateAlways() {
        }

        /// <summary>
        /// Used to update sound only while playing. Useful for changing sounds on the fly.
        /// Called after UpdatePlaying.
        /// </summary>
        protected virtual void UpdatePlaying() {
        }

        protected sealed override void Update() {

            bool playing = IsAudible() && IsPlaying();

            UpdateAlways();

            source.pitch = PitchReal;
            if (SoundController.TimescaleAffectsPitch) {
                source.pitch *= Time.timeScale;
            }

            if (playing) {

                UpdatePlaying();

                if (!source.isPlaying) {
                    if (!soundStarted) {
                        source.Play();
                        source.time = UnityEngine.Random.Range(0.0f, source.clip.length);
                        soundStarted = true;
                    }
                    source.UnPause();
                }

                FadeLevel = Mathf.Lerp(FadeLevel, 1, fadeDelta * Time.deltaTime);

                Sound.ActiveVolumeTotal -= VolumeReal;
                VolumeReal = volumeBase * Attenuation(volumeBase) * FadeLevel;
                Sound.ActiveVolumeTotal += VolumeReal;

                if (Time.timeScale > 0.01f) {
                    source.volume = VolumeReal * Saturation();
                }
                else {
                    source.volume = 0f;
                }
            }
            else if (!playing && source.volume > 0.01f) {

                FadeLevel = Mathf.Lerp(FadeLevel, 0, fadeDelta * Time.deltaTime);

                Sound.ActiveVolumeTotal -= VolumeReal;
                VolumeReal = volumeBase * Attenuation(volumeBase) * FadeLevel;
                Sound.ActiveVolumeTotal += VolumeReal;

                source.volume = VolumeReal * Saturation();
            }
            else if (!playing && source.isPlaying) {
                FadeLevel = 0;
                source.Pause();
                Sound.ActiveVolumeTotal -= VolumeReal;
                VolumeReal = 0;
            }
        }

    }
}
