﻿using ObjectExplorer.Mappings;
using UnityEngine;

namespace MoreSound {

    public class Mapper : MonoBehaviour {

        void Awake() {

            ObjectExplorer.ObjectExplorer.AddMappings("MoreSound",
                new MFloat<Sound>("VolumeReal", c => c.VolumeReal),
                new MFloat<Sound>("PitchReal", c => c.PitchReal),
                new MFloat<Sound>("DistanceCutoff", c => c.DistanceCutoff),
                new MFloat<Sound>("FadeLevel", c => c.FadeLevel),
                new MFloat<Sound>("Attenuation", c => c.Attenuation(c.VolumeReal)),
                new MBool<Sound>("IsAudible", c => c.IsAudible()),

                new MBool<SoundController>("TimescaleAffectsPitch", c => SoundController.TimescaleAffectsPitch, (c, x) => SoundController.TimescaleAffectsPitch = x),
                new MFloat<SoundController>("ActiveVolumeTotal", c => Sound.ActiveVolumeTotal),
                new MFloat<SoundController>("Saturation", c => Sound.Saturation()),
                new MIterable<SoundController, int>("ActiveCollisionIndices", c => Collision.ActiveCollisionIndices)
            );

        }
    }

}
