﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MoreSound {
    class ActionCannon : ActionSingle {

        public static ActionCannon Add(BlockBehaviour b) {
            float power = Mathf.Abs((b.BuildingBlock as CanonBlock).StrengthSlider.Value);

            if (b.BlockID == (int)BlockType.ShrapnelCannon) {

                return ActionSingle.Add<ActionCannon>(b,
                    sound: (b as CanonBlock).audioSource.clip,
                    volume: 0.3f,
                    pitch: (power < 4f) ? 1f : Mathf.Clamp(1.1f + ((1 - power) * 0.03f), 0.5f, 1),
                    source: (b as CanonBlock).audioSource);
            }
            else {

                return ActionSingle.Add<ActionCannon>(b,
                    sound: (power < 4f) ? (b as CanonBlock).audioSource.clip : Sound.cannonSound,
                    volume: 0.3f,
                    pitch: (power < 4f) ? 1f : Mathf.Clamp(3f - ((power - 4) * 0.13f), 0.5f, 3f),
                    source: (b as CanonBlock).audioSource);
            }

        }
    }
}
