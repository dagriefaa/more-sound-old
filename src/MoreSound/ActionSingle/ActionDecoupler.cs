﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MoreSound {
    class ActionDecoupler : MonoBehaviour {

        ExplosiveBolt bolt;
        Break breakSound;
        ConfigurableJoint joint;

        bool wasJoint = true;

        void Start() {
            bolt = GetComponent<ExplosiveBolt>();
            breakSound = GetComponent<Break>();
            joint = GetComponent<ConfigurableJoint>();
        }

        void Update() {
            if (!joint && wasJoint) {
                breakSound.OnJointBreak(8250);
                wasJoint = false;
            }
        }
    }
}
