﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace MoreSound {
    /// <summary>
    /// ActionContinuous implementation for CogMotorControllerHinge types.
    /// </summary>
    class Fire : ActionContinuous {

        FireController fire;

        public static Fire Add(BlockBehaviour b) {
            if (!b.GetComponent<FireController>()) {
                return null;
            }
            return ActionContinuous.Add<Fire>(b, Sound.fireSound, 0.2f);
        }

        void Start() {
            fire = GetComponentInChildren<FireController>();
        }

        protected override bool IsPlaying() {
            return fire.enabled;
        }

    }
}
